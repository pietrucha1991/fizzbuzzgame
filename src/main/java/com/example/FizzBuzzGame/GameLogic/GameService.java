package com.example.FizzBuzzGame.GameLogic;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GameService {

    final String fizzResult = "Fizz";
    final String buzzResult = "Buzz";
    final String buzzFizzResult = "Fizz Buzz";
    final String DELIMITER_CONST = ", ";

    public String getFizzBuzz(int number) {

        if (number % 15 == 0) {
            return buzzFizzResult;
        }
        if (number % 5 == 0) {
            return buzzResult;
        }
        if (number % 3 == 0) {
            return fizzResult;
        } else {
            return String.valueOf(number);
        }
    }

    public String getFizzBuzz(String numbers) {

        String[] numberStringArray = numbers.split(DELIMITER_CONST);
        List<String> stringList = new ArrayList<>();

        for (int i = 0; i < numberStringArray.length; i++) {
            String result = getFizzBuzz(Integer.parseInt(numberStringArray[i]));
            stringList.add(result);
        }

        return String.join(DELIMITER_CONST, stringList);
    }
}
