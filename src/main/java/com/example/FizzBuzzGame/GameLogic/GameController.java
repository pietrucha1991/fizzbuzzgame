package com.example.FizzBuzzGame.GameLogic;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("game")
public class GameController {

    private GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("oneInput")
    public String getResultWithOnlyOneNumberInput(int number){
        return gameService.getFizzBuzz(number);
    }

}
