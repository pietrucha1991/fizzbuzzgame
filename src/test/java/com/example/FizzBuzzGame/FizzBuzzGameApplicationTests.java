package com.example.FizzBuzzGame;

import com.example.FizzBuzzGame.GameLogic.GameService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FizzBuzzGameApplicationTests {

    @Autowired
    GameService gameService;

    @ParameterizedTest
    @MethodSource("provideDataForSingleInput")
    void getResultsForSingleInput(final int input, final String expected) {
        //given, when
        final String result = gameService.getFizzBuzz(input);
        //then
        assertThat(result).isEqualTo(expected);
    }

    private static Stream<Arguments> provideDataForSingleInput() {
        return Stream.of(
                Arguments.of(1, "1"),
                Arguments.of(2, "2"),
                Arguments.of(3, "Fizz"),
                Arguments.of(4, "4"),
                Arguments.of(5, "Buzz"),
                Arguments.of(6, "Fizz"),
                Arguments.of(7, "7"),
                Arguments.of(8, "8"),
                Arguments.of(9, "Fizz"),
                Arguments.of(10, "Buzz"),
                Arguments.of(11, "11"),
                Arguments.of(12, "Fizz"),
                Arguments.of(13, "13"),
                Arguments.of(14, "14"),
                Arguments.of(15, "Fizz Buzz")
        );
    }

    @Test
    void getResultsForMultipleInput() {
        //given
        final String input = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30";
        //when
        final String result = gameService.getFizzBuzz(input);
        //then
        final String expected = "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 26, Fizz, 28, 29, Fizz Buzz";
        assertThat(result).isEqualTo(expected);
    }
}
